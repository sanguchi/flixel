package;

import openfl.geom.Point;
import flixel.addons.util.FlxFSM;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.util.FlxAxes;
import Utils.KeyMapping;
import flixel.input.keyboard.FlxKey;
import flixel.input.FlxInput.FlxInputState;
import flash.filters.ColorMatrixFilter;
import flixel.util.FlxTimer;

class Slime extends FlxSprite
{
	public static inline var GRAVITY:Float = 600;
	
	public var fsm:FlxFSM<FlxSprite>;
	public static var DEFAULT_KEYMAPPING:KeyMapping = { 
		UP_KEY: FlxKey.UP,
		RIGHT_KEY: FlxKey.RIGHT, 
		DOWN_KEY: FlxKey.DOWN, 
		LEFT_KEY: FlxKey.LEFT 
		}; 

	public var customKeyMap:KeyMapping;

	public function new(X:Float = 0, Y:Float = 0, ?hueRotation:Int = 0)
	{
		super(X, Y);
		customKeyMap = DEFAULT_KEYMAPPING;
		loadGraphic("assets/slime.png", true, 16, 16, true);
		loadGraphic(rotateHue(hueRotation), true, 16, 16, true);
		setFacingFlip(FlxObject.LEFT, true, false);
		setFacingFlip(FlxObject.RIGHT, false, false);
		facing = FlxObject.RIGHT;
		
		addAnimations();
		
		acceleration.y = GRAVITY;
		maxVelocity.set(100, GRAVITY);
		
		fsm = new FlxFSM<FlxSprite>(this);
		fsm.transitions
			.add(Idle, Jump, Conditions.jump)
			.add(Idle, CrushedAnimation, Conditions.gotCrushed)
			.add(CrushedAnimation, DyingAnimation, Conditions.animationFinished)
			.add(Jump, Idle, Conditions.grounded)
			.add(Jump, GroundPound, Conditions.groundSlam)
			.add(GroundPound, GroundPoundFinish, Conditions.grounded)
			.add(GroundPoundFinish, Idle, Conditions.animationFinished)
			.start(Idle);
	}

	private function addAnimations():Void
	{
		animation.add("standing", [0, 1], 3);
		animation.add("walking", [0, 1], 12);
		animation.add("jumping", [2]);
		animation.add("pound", [3]);
		animation.add("landing", [4, 0, 1, 0], 8, false);
		animation.add("crushed", [1,4], 6, false);
		animation.add("dying", [4,5], 12);
	}

	override public function update(elapsed:Float):Void 
	{
		fsm.update(elapsed);
		super.update(elapsed);
		levelWarpUpdate();
	}
	
	override public function destroy():Void 
	{
		fsm.destroy();
		fsm = null;
		super.destroy();
	}

	private function levelWarpUpdate():Void
	{
		if(y > 240){
			y = 1;
		}
	}
	public function checkCrushed(player:Slime, other:Slime):Void
	{
		if(other.fsm.stateClass == GroundPound) {
			player.fsm.state = new CrushedAnimation();
		}
	}

	public function respawn(Timer:FlxTimer):Void
	{
		reset(FlxG.random.int(2, 18) * 16, 16);
		fsm.state = new Idle();
	}


	public function setKeyMapping(keys:Utils.KeyMapping):Void
	{
		customKeyMap = keys;
	}

	public function rotateHue(hueRotation:Int):openfl.display.BitmapData
	{
		// Since we're reusing the same image, clone it so that pixel data changes
		// for each copy of the sprite
		var data = pixels.clone();
		pixels = data;

		// http://stackoverflow.com/questions/8507885/shift-hue-of-an-rgb-color
		// -hueRotation to match GIMP's direction of rotation
		var cosA:Float = Math.cos(-hueRotation * Math.PI / 180);
		var sinA:Float = Math.sin(-hueRotation * Math.PI / 180);

		var a1:Float =	cosA + (1.0 - cosA) / 3.0;
		var a2:Float =	1.0/3.0 * (1.0 - cosA) - Math.sqrt(1.0/3.0) * sinA;
		var a3:Float =	1.0/3.0 * (1.0 - cosA) + Math.sqrt(1.0/3.0) * sinA;

		var b1:Float =	a3;
		var b2:Float = 	cosA + 1.0/3.0 * (1.0 - cosA);
		var b3:Float =	a2;

		var c1:Float =	a2;
		var c2:Float =	a3;
		var c3:Float = 	b2;

		pixels.applyFilter(pixels, pixels.rect, new Point(), new ColorMatrixFilter(
			[a1, b1, c1, 0, 0,
			a2, b2, c2, 0, 0,
			a3, b3, c3, 0, 0,
			0, 0, 0, 1, 0])); // identity row

		#if flash
			dirty = true; // make this work on Flash, too
		#end
		// addAnimations();
		return data;
	}

}

class Conditions
{
	public static function jump(Owner:FlxSprite):Bool
	{
		// return (FlxG.keys.justPressed.UP && Owner.isTouching(FlxObject.DOWN));
		var SlimeOwner:Slime = cast(Owner, Slime); 
		return (FlxG.keys.checkStatus(SlimeOwner.customKeyMap.UP_KEY, FlxInputState.JUST_PRESSED) && Owner.isTouching(FlxObject.DOWN));
	}
	
	public static function grounded(Owner:FlxSprite):Bool
	{
		return Owner.isTouching(FlxObject.DOWN);
	}
	
	public static function groundSlam(Owner:FlxSprite):Bool
	{
		// return FlxG.keys.justPressed.DOWN && !Owner.isTouching(FlxObject.DOWN);
		var SlimeOwner:Slime = cast(Owner, Slime); 
		return (FlxG.keys.checkStatus(SlimeOwner.customKeyMap.DOWN_KEY, FlxInputState.JUST_PRESSED) && !Owner.isTouching(FlxObject.DOWN));
	}
	
	public static function animationFinished(Owner:FlxSprite):Bool
	{
		return Owner.animation.finished;
	}

	public static function gotCrushed(Owner:FlxSprite): Bool
	{
		// return Owner.overlaps(Slime);
		// return FlxG.overlap(Owner, Slime);
		return false;
	}
}

class Idle extends FlxFSMState<FlxSprite>
{
	override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.animation.play("standing");
	}
	
	override public function update(elapsed:Float, owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.acceleration.x = 0;
		/*
		if (FlxG.keys.pressed.LEFT || FlxG.keys.pressed.RIGHT)
		{
			owner.facing = FlxG.keys.pressed.LEFT ? FlxObject.LEFT : FlxObject.RIGHT;
			owner.animation.play("walking");
			owner.acceleration.x = FlxG.keys.pressed.LEFT ? -300 : 300;
		}
		*/
		var SlimeOwner:Slime = cast(owner, Slime);
		if (FlxG.keys.checkStatus(SlimeOwner.customKeyMap.LEFT_KEY, FlxInputState.PRESSED) || FlxG.keys.checkStatus(SlimeOwner.customKeyMap.RIGHT_KEY, FlxInputState.PRESSED))
		{
			owner.facing = FlxG.keys.checkStatus(SlimeOwner.customKeyMap.LEFT_KEY, FlxInputState.PRESSED) ? FlxObject.LEFT : FlxObject.RIGHT;
			owner.animation.play("walking");
			owner.acceleration.x = FlxG.keys.checkStatus(SlimeOwner.customKeyMap.LEFT_KEY, FlxInputState.PRESSED) ? -300 : 300;
		}
		else
		{
			owner.animation.play("standing");
			owner.velocity.x *= 0.9;	
		}
	}
}

class Jump extends FlxFSMState<FlxSprite>
{
	override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.animation.play("jumping");
		owner.velocity.y = -200;
	}
	
	override public function update(elapsed:Float, owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.acceleration.x = 0;
		/*
		if (FlxG.keys.pressed.LEFT || FlxG.keys.pressed.RIGHT)
		{
			owner.acceleration.x = FlxG.keys.pressed.LEFT ? -300 : 300;
			owner.facing = FlxG.keys.pressed.LEFT ? FlxObject.LEFT : FlxObject.RIGHT;
		}
		*/
		var SlimeOwner:Slime = cast(owner, Slime);
		if (FlxG.keys.checkStatus(SlimeOwner.customKeyMap.LEFT_KEY, FlxInputState.PRESSED) || FlxG.keys.checkStatus(SlimeOwner.customKeyMap.RIGHT_KEY, FlxInputState.PRESSED))
		{
			owner.acceleration.x = FlxG.keys.checkStatus(SlimeOwner.customKeyMap.LEFT_KEY, FlxInputState.PRESSED) ? -300 : 300;
			owner.facing = FlxG.keys.checkStatus(SlimeOwner.customKeyMap.LEFT_KEY, FlxInputState.PRESSED) ? FlxObject.LEFT : FlxObject.RIGHT;
		}



	}
}

class SuperJump extends Jump
{
	override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.animation.play("jumping");
		owner.velocity.y = -300;
	}
}

class GroundPound extends FlxFSMState<FlxSprite>
{
	private var _ticks:Float;
	
	override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.animation.play("pound");
		owner.velocity.x = 0;
		owner.acceleration.x = 0;
		_ticks = 0;
	}
	
	override public function update(elapsed:Float, owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		_ticks++;
		if (_ticks < 15)
		{
			owner.velocity.y = 0;
		}
		else
		{
			owner.velocity.y = Slime.GRAVITY;
		}
	}
}

class GroundPoundFinish extends FlxFSMState<FlxSprite>
{
	override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.animation.play("landing");
		FlxG.camera.shake(0.025, 0.25, null, false, FlxAxes.Y);
		owner.velocity.x = 0;
		owner.acceleration.x = 0;
	}
}


class CrushedAnimation extends FlxFSMState<FlxSprite>
{
	override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void
	{
		owner.animation.play("crushed");
	}
}

class DyingAnimation extends FlxFSMState<FlxSprite>
{
	private var _ticks:Float;
	
	override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.animation.play("dying");
		// owner.velocity.y = -100;
		_ticks = 0;
	}
	
	override public function update(elapsed:Float, owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		_ticks++;
		if(_ticks > 200){
			owner.kill();
			var SlimeOwner:Slime = cast(owner, Slime);
			new FlxTimer().start(3, SlimeOwner.respawn);
		}
	}
}

