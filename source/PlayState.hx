package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.tile.FlxTilemap;
import flixel.util.FlxColor;
using StringTools;
import flixel.group.FlxSpriteGroup;
import flixel.input.keyboard.FlxKeyboard;
import flixel.input.keyboard.FlxKey;
import flixel.util.FlxTimer;

class PlayState extends FlxState
{
	private var _map:FlxTilemap;
	private var _slime:Slime;
	private var _slime2:Slime;
	private var _powerup:FlxSprite;
	// private var _enemy:Enemy;
	private var _rotations:Int = 1;
	private var _players:FlxSpriteGroup = new FlxSpriteGroup(8);
	/*
	private var _info:String = "LEFT & RIGHT to move, UP to jump\nDOWN (in the air) " +
		"to ground-pound.\nR to Reset\n\nCurrent State: {STATE}\nSlime.x/y {POS}, State {STATE}";
	*/
	private var _info:String = "Player 1 Keys: Arrows - Player 2 Keys: WASD\nDOWN (in the air) to ground-pound.\nR to Reset";
	private var _txtInfo:FlxText;
	
	override public function create():Void
	{
		bgColor = 0xff661166;
		super.create();
		
		_map = new FlxTilemap();
		_map.loadMapFromArray([
			1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
			1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
			1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
			1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
			1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
			1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
			1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
			1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
			1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
			1,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,1,
			1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
			1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,1,1,1,
			1,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,1,1,1,1,
			1,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,1,1,1,1,
			1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
			20, 15, "assets/tiles.png", 16, 16);
		add(_map);
		
		_slime = new Slime(192, 128);
		_slime2 = new Slime(192, 96, 100);
		_slime2.setKeyMapping({UP_KEY: FlxKey.W, RIGHT_KEY: FlxKey.D, DOWN_KEY: FlxKey.S, LEFT_KEY: FlxKey.A});
		
		_players.add(_slime);
		_players.add(_slime2);
		add(_players);
		

		// ENEMY
		// _enemy = new Enemy(32, 32); 
		// add(_enemy);

		_powerup = new FlxSprite(48, 208, "assets/powerup.png");
		add(_powerup);
		
		_txtInfo = new FlxText(16, 16, -1, _info);
		add(_txtInfo);
	}
	
	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		// _rotations++;
		// _slime2.rotateHue(1);
		FlxG.collide(_map, _players);
		FlxG.collide(_players, _players);
		// FlxG.collide(_map, _enemy);
		FlxG.overlap(_players, _powerup, getPowerup);
		FlxG.overlap(_players, _players, crushTest);
		// FlxG.overlap(_enemy, _slime, _enemy.checkEnemy);

		/*
		_txtInfo.text = _info.replace("{STATE}", Type.getClassName(_slime.fsm.stateClass))
		.replace("{POS}", Std.string(_slime.x) + "," + Std.string(_slime.y))
		.replace("{STATE}", Std.string(_slime.fsm.state));
		*/
		
		_txtInfo.text = _info;

		if (FlxG.keys.justReleased.R)
		{
			FlxG.camera.flash(FlxColor.BLACK, .1, FlxG.resetState);
		}
	}
	
	private function getPowerup(slime:Slime, particle:FlxSprite):Void
	{		
		slime.fsm.transitions.replace(Slime.Jump, Slime.SuperJump);
		slime.fsm.transitions.add(Slime.Jump, Slime.Idle, Slime.Conditions.grounded);
		
		particle.kill();
		new FlxTimer().start(30, respawnPowerUp);
	}

	private function respawnPowerUp(timer:FlxTimer):Void
	{
		_powerup = new FlxSprite(48, 208, "assets/powerup.png");
		add(_powerup);
	}

	private function crushTest(slime1:Slime, slime2:Slime):Void
	{
		var pounder:Null<Slime> = null;
		for(slime in [slime1, slime2]){
			if(slime.fsm.stateClass == Slime.GroundPound) {
				pounder = slime;
			}
		}
		if(pounder != null){
			for(slime in [slime1, slime2]){
				if(slime != pounder) {
					slime.fsm.state = new Slime.CrushedAnimation();
				}
			}
		}
		
	}
}
