package;
import flixel.input.keyboard.FlxKey;

typedef KeyMapping = {
  var UP_KEY : FlxKey;
  var DOWN_KEY : FlxKey;
  var LEFT_KEY : FlxKey;
  var RIGHT_KEY: FlxKey;
}